extern crate ncurses;
extern crate rand;
extern crate num;

use ncurses::*;
use rand::Rng;


struct Position {
    x: u32,
    y: u32,
    // tile: TILE_TYPE,
}

struct Room {
    position: Position,
    width: u32,
    height: u32,
    doors: Vec<Position>,
    // monsters: Monster,
    // items: Intem
}

struct Player {
    position: Position,
    health: i32,
    // room: Room,
}

impl Player {
    pub fn new(x: u32, y: u32, health: i32) -> Player {
        Player {
            position: Position { x, y },
            health: health,
        }
    }
}

impl Room {
    pub fn new(x: u32, y: u32, width: u32, height: u32) -> Room {
        Room {
            position: Position { x, y },
            width: width,
            height: height,
            doors: Vec::with_capacity(4),
        }
    }
}

fn create_room(xPos: u32, yPos: u32, width: u32, height: u32) -> Room {
    let mut new_room = Room::new(xPos, yPos, width, height);

    let x = 0;
    let y = 0;

    // top door
    let p = Position { x, y };
    new_room.doors.push(p);
    new_room.doors[0].x = rand::random::<u32>() % (width - 2) + new_room.position.x + 1;
    new_room.doors[0].y = new_room.position.y;

    // left door
    let p = Position { x, y };
    new_room.doors.push(p);
    new_room.doors[1].y = rand::random::<u32>() % (height - 2) + new_room.position.y + 1;
    new_room.doors[1].x = new_room.position.x;


    // bottom door
    let p = Position { x, y };
    new_room.doors.push(p);
    new_room.doors[2].x = rand::random::<u32>() % (width - 2) + new_room.position.x + 1;
    new_room.doors[2].y = new_room.position.y + new_room.height;


    // right door
    let p = Position { x, y };
    new_room.doors.push(p);
    new_room.doors[3].y = rand::random::<u32>() % (height - 2) + new_room.position.y + 1;
    new_room.doors[3].x = new_room.position.x + width - 1;

    new_room
}

fn draw_rooom(room: &mut Room) -> bool {
    let x = room.position.x;
    let y = room.position.y + 1;

    // draw wall and floor
    'top_borrom: for x in x..room.position.x + room.width {
        mvprintw(room.position.y as i32, x as i32, "-"); // top
        mvprintw((room.position.y + room.height) as i32, x as i32, "-"); // bottom
    }

    'side_wall: for y in y..room.position.y + room.height {
        mvprintw(y as i32, room.position.x as i32, "|"); // top
        mvprintw(y as i32, (room.position.x + room.width - 1) as i32, "|"); // bottom

        'floor: for x in room.position.x + 1..room.position.x + room.width - 1 {
            mvprintw(y as i32, x as i32, ".");
        }
    }

    // draw doors
    mvprintw(room.doors[0].y as i32, room.doors[0].x as i32, "+");
    mvprintw(room.doors[1].y as i32, room.doors[1].x as i32, "+");
    mvprintw(room.doors[2].y as i32, room.doors[2].x as i32, "+");
    mvprintw(room.doors[3].y as i32, room.doors[3].x as i32, "+");

    true
}

fn connect_doors(door_one: &mut Position, door_two: &mut Position) -> bool {

    let mut temp = Position {
        x: door_one.x,
        y: door_one.y,
    };

    loop {
        // step left
        if (((temp.x - 1) - door_two.x) < (temp.x - door_two.x)) &&
            (mvinch(temp.y as i32, (temp.x - 1) as i32) == ' ' as u32)
        {
            mvprintw(temp.y as i32, (temp.x - 1) as i32, "#");
            temp.x = temp.x - 1;
        // step right
        } else if (((temp.x + 1) - door_two.x) < (temp.x - door_two.x)) &&
                   (mvinch(temp.y as i32, (temp.x + 1) as i32) == ' ' as u32)
        {
            mvprintw(temp.y as i32, (temp.x + 1) as i32, "#");
            temp.x = temp.x + 1;
        // step down
        } else if (((temp.y + 1) - door_two.y) < (temp.y - door_two.y)) &&
                   (mvinch((temp.y + 1) as i32, temp.x as i32) == ' ' as u32)
        {
            mvprintw((temp.y + 1) as i32, temp.x as i32, "#");
            temp.y = temp.y + 1;
        // step up
        } else if (((temp.y - 1) - door_two.y) < (temp.y - door_two.y)) &&
                   (mvinch((temp.y - 1) as i32, temp.x as i32) == ' ' as u32)
        {
            mvprintw((temp.y - 1) as i32, temp.x as i32, "#");
            temp.y = temp.y - 1;
        } else {
            return false;
            break;
        }
        getch();
    }

    true
}

fn main() {

    screen_setup();
    map_setup();


    let mut user: Player = player_setup();
    loop {
        let ch = getch() as u8 as char;
        match ch {
            'q' | 'Q' => {}
            _ => {
                handle_input(ch, &mut user);
                continue;
            }
        };
        break;
    }
    endwin();

}

fn screen_setup() -> i32 {
    initscr();
    printw("Hello, world!");
    noecho();
    refresh();
    return 0;
}

fn map_setup() -> Vec<Room> {

    let mut rooms: Vec<Room> = Vec::with_capacity(6);
    // mvprintw(13, 13, "---------");
    // mvprintw(14, 13, "|.......|");
    // mvprintw(15, 13, "|.......|");
    // mvprintw(16, 13, "|.......|");
    // mvprintw(17, 13, "|.......|");
    // mvprintw(18, 13, "---------");

    let room = create_room(13, 13, 12, 8);
    rooms.push(room);
    draw_rooom(&mut rooms[0]);
    //
    // mvprintw(2, 40, "---------");
    // mvprintw(3, 40, "|.......|");
    // mvprintw(4, 40, "|.......|");
    // mvprintw(5, 40, "|.......|");
    // mvprintw(6, 40, "|.......|");
    // mvprintw(7, 40, "---------");

    let room = create_room(40, 2, 8, 8);
    rooms.push(room);
    draw_rooom(&mut rooms[1]);
    //
    // mvprintw(10, 40, "-------------");
    // mvprintw(11, 40, "|...........|");
    // mvprintw(12, 40, "|...........|");
    // mvprintw(13, 40, "|...........|");
    // mvprintw(14, 40, "|...........|");
    // mvprintw(15, 40, "-------------");

    let room = create_room(40, 12, 10, 10);
    rooms.push(room);
    draw_rooom(&mut rooms[2]);
    //
    // mvprintw(15, 60, "---------------");
    // mvprintw(16, 60, "|.............|");
    // mvprintw(17, 60, "|.............|");
    // mvprintw(18, 60, "|.............|");
    // mvprintw(19, 60, "|.............|");
    // mvprintw(20, 60, "---------------");
    //
    // mvprintw(13, 25, "-------------");
    // mvprintw(14, 25, "|...........|");
    // mvprintw(15, 25, "|...........|");
    // mvprintw(16, 25, "|...........|");
    // mvprintw(17, 25, "|...........|");
    // mvprintw(18, 25, "-------------");

    connect_doors(&mut rooms[0].doors[3], &mut rooms[2].doors[1]);
    return rooms;
}

fn player_setup() -> Player {

    let mut newPlayer = Player::new(14, 14, 20);

    player_move(14, 14, &mut newPlayer);
    mv(14, 14);
    newPlayer
}

fn handle_input(ch: char, usr: &mut Player) {

    let mut newY = 0;
    let mut newX = 0;

    match ch {
        'w' | 'W' => {
            newY = usr.position.y - 1;
            newX = usr.position.x;
        }
        's' | 'S' => {
            newY = usr.position.y + 1;
            newX = usr.position.x;
        }
        'a' | 'A' => {
            newY = usr.position.y;
            newX = usr.position.x - 1;
        }
        'd' | 'D' => {
            newY = usr.position.y;
            newX = usr.position.x + 1;
        }
        _ => {}
    }

    check_position(newY, newX, usr);
}

fn check_position(newY: u32, newX: u32, mut user: &mut Player) {

    // let space;

    match mvinch(newY as i32, newX as i32) as u8 as char {
        '.' => {
            player_move(newY, newX, &mut user);
            mv(user.position.y as i32, user.position.x as i32);
        }
        '+' => {
            player_move(newY, newX, &mut user);
            mv(user.position.y as i32, user.position.x as i32);
        }
        '#' => {
            player_move(newY, newX, &mut user);
            mv(user.position.y as i32, user.position.x as i32);
        }
        _ => {
            mv(user.position.y as i32, user.position.x as i32);
        }
    }

}

fn player_move(y: u32, x: u32, user: &mut Player) {
    mvprintw(user.position.y as i32, user.position.x as i32, ".");
    user.position.y = y;
    user.position.x = x;
    mvprintw(user.position.y as i32, user.position.x as i32, "@");
    mv(user.position.y as i32, user.position.x as i32);
}
